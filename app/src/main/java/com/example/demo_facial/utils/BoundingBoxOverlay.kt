package com.example.demo_facial.utils
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import androidx.camera.core.CameraSelector
import androidx.core.graphics.toRectF
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceLandmark

class BoundingBoxOverlay( context: Context , attributeSet: AttributeSet )
    : SurfaceView( context , attributeSet ) , SurfaceHolder.Callback {

    companion object{
        val TAG = "box"
    }

    var areDimsInit:Boolean = false
    var no_limpiar:Boolean = true
    var frameHeight = 0
    var frameWidth = 0
    var cameraFacing : Int = CameraSelector.LENS_FACING_FRONT
    var faceBoundingBox: Prediction? = null
    var drawMaskLabel = true
    var face:Face? = null
    init {
        holder.setFormat(PixelFormat.TRANSLUCENT)
        setZOrderOnTop(true)
        holder.addCallback(this)
        setWillNotDraw(false)
    }

    private var output2OverlayTransform: Matrix = Matrix()
    private val boxPaint = Paint().apply {
        color = Color.parseColor("#7822CC12")
        style = Paint.Style.FILL
    }
    private val pointfacial = Paint().apply {
        color = Color.parseColor("#3e4ee9f9")
        style = Paint.Style.FILL
    }
    private val textPaint = Paint().apply {
        strokeWidth = 2.0f
        textSize =32f
        color = Color.BLUE
    }
    override fun surfaceCreated(holder: SurfaceHolder) {

    }
    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {

    }
    override fun surfaceDestroyed(holder: SurfaceHolder) {

    }

    private fun drawLandmarks(canvas: Canvas?) {
        val leftEyeLandmark = face!!.getLandmark(FaceLandmark.LEFT_EYE)
        val rightEyeLandmark = face!!.getLandmark(FaceLandmark.RIGHT_EYE)
        val noseBaseLandmark = face!!.getLandmark(FaceLandmark.NOSE_BASE)

        val paint = Paint()
        paint.color = Color.RED
        paint.style = Paint.Style.FILL
        paint.strokeWidth = 5f

        if (leftEyeLandmark != null && rightEyeLandmark != null && noseBaseLandmark != null) {
            val leftEyePos = leftEyeLandmark.position
            val rightEyePos = rightEyeLandmark.position
            val noseBasePos = noseBaseLandmark.position
            Log.i(TAG,"dibujando landmark")
            canvas!!.drawCircle(leftEyePos.x, leftEyePos.y, 10f, paint)
            canvas!!.drawCircle(rightEyePos.x, rightEyePos.y, 10f, paint)
            canvas!!.drawCircle(noseBasePos.x, noseBasePos.y, 10f, paint)
        }
    }
    fun clearCanvas() {
        val canvas = holder.lockCanvas() ?: return
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        holder.unlockCanvasAndPost(canvas)
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
            if (faceBoundingBox != null) {
                if (!areDimsInit) {
                    val viewWidth = canvas!!.width.toFloat()
                    val viewHeight = canvas.height.toFloat()
                    val xFactor: Float = viewWidth / frameWidth.toFloat()
                    val yFactor: Float = viewHeight / frameHeight.toFloat()
                    output2OverlayTransform.preScale(xFactor, yFactor)
                    if (cameraFacing == CameraSelector.LENS_FACING_FRONT) {
                        output2OverlayTransform.postScale(-1f, 1f, viewWidth / 2f, viewHeight / 2f)
                    }
                    areDimsInit = true
                } else {
                    val boundingBox = faceBoundingBox!!.bbox.toRectF()
                    output2OverlayTransform.mapRect(boundingBox)
                    canvas?.drawRoundRect(boundingBox, 16f, 16f, boxPaint)
                    canvas?.drawText(
                        faceBoundingBox!!.label,
                        boundingBox.centerX() / 2,
                        boundingBox.centerY() + 50,
                        textPaint
                    )

                }
        }
    }
    }