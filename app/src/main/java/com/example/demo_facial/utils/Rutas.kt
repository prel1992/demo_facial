package com.example.demo_facial.utils

import android.graphics.Bitmap
import android.util.Base64
import android.util.Log
import android.widget.Toast
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException

class Rutas {
    companion object{
        var repta1:String = ""
        var repta2:String = ""
        private fun ObtenerVector(dni:String){
            val client = OkHttpClient()
            val mediaType = "application/json; charset=utf-8".toMediaType()
            val body = "{\"dni\": \"$dni\"}".toRequestBody(mediaType)
            val url = "http://ec2-3-84-101-12.compute-1.amazonaws.com/api/v1.0/facialbiometric/obtener/"
            val request: Request = Request.Builder()
                .url(url)
                .post(body) // Cambia el método de solicitud a POST
                .addHeader("Content-Type", "application/json")
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {
                        val responseData: String? = response.body?.string()
                        if (responseData != null) {
                            repta1 = responseData
                        }
                    }
                }
            })
        }

        private fun sendJsonData(dni: String,base64Image:String) {
            val client = OkHttpClient()
            val jsonData = JSONObject()
            jsonData.put("base64_image", base64Image)
            jsonData.put("dni",dni)
            //Log.i("base64_image",base64Image)
            val mediaType = "application/json; charset=utf-8".toMediaType()
            val body = jsonData.toString().toRequestBody(mediaType)

            val request: Request = Request.Builder()
                .url("http://ec2-3-84-101-12.compute-1.amazonaws.com/api/v1.0/facialbiometric/validar/")
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }
                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {
                        val responseData: String? = response.body?.string()
                        if (responseData != null) {
                            val jsonObject = JSONObject(responseData)
                            if (jsonObject.has("valid")) {
                                val validValue = jsonObject.getString("valid")
                                repta2 = validValue
                            }
                        }
                    }
                }
            })
        }
        private fun encodeImageToBase64(bitmap: Bitmap): String {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            return Base64.encodeToString(byteArray, Base64.DEFAULT)
        }
    }
}