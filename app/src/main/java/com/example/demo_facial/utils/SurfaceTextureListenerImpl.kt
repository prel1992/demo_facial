package com.example.demo_facial.utils

import android.graphics.SurfaceTexture
import android.view.TextureView

class SurfaceTextureListenerImpl(private val onSurfaceTextureReady: (SurfaceTexture) -> Unit) : TextureView.SurfaceTextureListener {
    override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
        onSurfaceTextureReady(surface)
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {}

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
        return false
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {}
}
