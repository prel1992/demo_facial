package com.example.demo_facial.utils

import android.annotation.SuppressLint
import android.graphics.*
import android.util.Log
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream

class DetectEyes(private var boundingBoxOverlay: BoundingBoxOverlay,private val onEyeStateChangeListener: OnEyeStateChangeListener) : ImageAnalysis.Analyzer {
    var isProcessing: Boolean = false
    var frame:Bitmap? = null
    var leftEyeOpenProbability  = 0.0f
    var rightEyeOpenProbability = 0.0f
    private var pred: Prediction? = null
    private val outStream = ByteArrayOutputStream() // Reutilización de objeto
    private val imageAnalysisDispatcher = Dispatchers.Default
    interface OnEyeStateChangeListener {
        fun onEyeStateDetected(isLeftEyeOpen: Boolean, isRightEyeOpen: Boolean)
    }

    private val faceDetector: FaceDetector
    init {
        val options = FaceDetectorOptions.Builder()
            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
            .build()
        faceDetector = FaceDetection.getClient(options)
    }
    suspend fun process(faces: List<Face>) {
        withContext(imageAnalysisDispatcher) {
            for(face in faces){
                try {
                    leftEyeOpenProbability = face.leftEyeOpenProbability!!
                    rightEyeOpenProbability = face.rightEyeOpenProbability!!
                    pred = Prediction(face.boundingBox, "", "")

                } catch (e: Exception) {
                    Log.e(TAG, "Exception in FrameAnalyzer: ${e.message}")
                    continue
                }
                withContext(Dispatchers.Main){
                    if (leftEyeOpenProbability != null && rightEyeOpenProbability != null && leftEyeOpenProbability != 0.0f && rightEyeOpenProbability != 0.0f) {
                            pred = Prediction(face.boundingBox, "Parpadeaste", "")
                            onEyeStateChangeListener.onEyeStateDetected(
                                leftEyeOpenProbability < 0.25,
                                rightEyeOpenProbability < 0.25
                            )

                    }
                    boundingBoxOverlay.faceBoundingBox = pred
                    boundingBoxOverlay.invalidate()
                }
            }

        }
    }

    @SuppressLint("UnsafeOptInUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        if (isProcessing) {
            val mediaImage = imageProxy.image
            if (mediaImage != null) {
                frame = Bitmap.createBitmap(
                    mediaImage.width,
                    mediaImage.height,
                    Bitmap.Config.ARGB_8888
                )
                frame!!.copyPixelsFromBuffer(imageProxy.planes[0].buffer)
                frame = BitmapUtils.rotateBitmap(frame!!, imageProxy.imageInfo.rotationDegrees.toFloat())
                if (!boundingBoxOverlay.areDimsInit) {
                    boundingBoxOverlay.frameHeight = frame!!.height
                    boundingBoxOverlay.frameWidth = frame!!.width
                }
                val inputImage = InputImage.fromBitmap(frame!!, 0)
                faceDetector.process(inputImage)
                    .addOnSuccessListener { faces ->
                        if (faces.size > 0) {
                            CoroutineScope(imageAnalysisDispatcher).launch {
                                process(faces)
                            }
                        }
                        imageProxy.close()
                    }
                    .addOnFailureListener { e ->
                        Log.e(TAG, "Error analyzing image: ${e.message}")
                        imageProxy.close()
                    }
            }
        }
        else {
            imageProxy.close()
            boundingBoxOverlay.areDimsInit = false
            //boundingBoxOverlay.no_limpiar = false
            boundingBoxOverlay.clearCanvas()

        }
    }

    companion object {
        private const val TAG = "EyeDetectionAnalyzer"
    }
}