package com.example.demo_facial

import android.Manifest
import android.Manifest.permission.CAMERA
import android.app.Dialog
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Base64
import android.util.Size
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.demo_facial.databinding.ActivityMainBinding
import com.example.demo_facial.utils.BoundingBoxOverlay
import com.example.demo_facial.utils.DetectEyes
import com.google.common.util.concurrent.ListenableFuture
import com.google.gson.annotations.SerializedName
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.io.ByteArrayOutputStream
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private lateinit var in_dni: EditText
    private lateinit var captureButton: Button
    private lateinit var PredictButton: Button
    private lateinit var txt_dni: TextView
    private lateinit var cameraPreview: Preview
    private var base64Image:String = ""
    private lateinit var imageAnalyzer: DetectEyes
    private lateinit var imageAnalysis:ImageAnalysis
    private lateinit var cameraSelector: CameraSelector
    private lateinit var  cameraProviderFuture: ListenableFuture<ProcessCameraProvider>
    private lateinit var processCameraProvider: ProcessCameraProvider
    private lateinit var  binding:ActivityMainBinding
    private val REQUEST_WRITE_STORAGE = 112
    private lateinit var progressDialog:Dialog
    private var boundingBoxOverlay: BoundingBoxOverlay? = null
    private var count:Int=0
    val okHttpClient = OkHttpClient.Builder()
        .connectTimeout(10, TimeUnit.SECONDS)  // Tiempo de espera para la conexión
        .readTimeout(10, TimeUnit.SECONDS)     // Tiempo de espera para la lectura de datos
        .writeTimeout(10, TimeUnit.SECONDS)    // Tiempo de espera para la escritura de datos
        .build()
    val retrofit = Retrofit.Builder()
        .baseUrl("http://ec2-3-84-101-12.compute-1.amazonaws.com")
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
    private companion object{
        val TAG:String = "Facial_Main"
    }
    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()){
        if(it){

        }
    }
    private fun requestWritePermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_WRITE_STORAGE
            )
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestPermissionLauncher.launch(CAMERA)
        requestPermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        in_dni = binding.inputDni
        txt_dni = binding.txtDni
        captureButton = binding.captureButton
        PredictButton = binding.predictButton
        boundingBoxOverlay = binding.bboxOverlay
        cameraSelector = CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()
        cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        progressDialog = Dialog(this@MainActivity).apply {
            setContentView(R.layout.progressbar)
            setCancelable(false)
        }
        cameraProviderFuture.addListener({
            processCameraProvider = cameraProviderFuture.get()
            cameraPreview = Preview.Builder()
                .build()
            imageAnalysis = ImageAnalysis.Builder()
                .setTargetResolution(Size(160, 240))
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_BLOCK_PRODUCER)
                .setOutputImageFormat(ImageAnalysis.OUTPUT_IMAGE_FORMAT_RGBA_8888)
                .build()
            imageAnalyzer = DetectEyes(boundingBoxOverlay!!,object : DetectEyes.OnEyeStateChangeListener {
                override fun onEyeStateDetected(isLeftEyeOpen: Boolean, isRightEyeOpen: Boolean) {
                    if (isLeftEyeOpen && isRightEyeOpen) {
                        imageAnalyzer.isProcessing = false
                        if(count==0) {
                            count += 1
                            validar()
                            progressDialog.show()

                        }
                    }
                }
            })
            imageAnalysis.setAnalyzer(Executors.newSingleThreadExecutor(), imageAnalyzer)
            processCameraProvider.unbindAll()
            processCameraProvider.bindToLifecycle(this,cameraSelector,cameraPreview,imageAnalysis)
            cameraPreview.setSurfaceProvider(binding.previewview.surfaceProvider)
        },ContextCompat.getMainExecutor(this))
        PredictButton.setOnClickListener{
            imageAnalyzer.isProcessing = true
            boundingBoxOverlay!!.no_limpiar = true
            txt_dni.text = "Parpadee y no retire su rostro\n mantengalo hasta recibir respuesta"
        }
        captureButton.setOnClickListener{
            takePhoto()
        }
    }

    private fun takePhoto() {
        val dni = in_dni.text.toString()
        if (dni.length == 8) {
            imageAnalyzer.isProcessing = false
            obtener(dni)
        } else {
            runOnUiThread {
                Toast.makeText(baseContext, "El texto debe tener 8 caracteres.", Toast.LENGTH_LONG).show()
            }
        }
    }
    fun bitmapToBase64(bitmap: Bitmap): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }
    private fun obtener(dni:String) {
        progressDialog.show()
        val apiService = retrofit.create(ApiService::class.java)
        val call = apiService.obtener(Data(dni,""))

        call.enqueue(object : Callback<MSG> {
            override fun onResponse(call: Call<MSG>, response: Response<MSG>) {
                val msg: MSG? = response.body()
                runOnUiThread {
                    progressDialog.dismiss()

                    if (msg != null) {
                        Toast.makeText(baseContext, "Rostro encontrado", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(baseContext, "no se obtuvo respuesta", Toast.LENGTH_LONG).show()
                    }
                }
            }
            override fun onFailure(call: Call<MSG>, t: Throwable) {
                runOnUiThread {
                    progressDialog.dismiss()
                    Toast.makeText(baseContext, "Rostro no encontrado", Toast.LENGTH_LONG).show()
                }
            }
        })
    }
    private fun validar() {
        progressDialog.show()
        val dni = in_dni.text.toString()
        base64Image = bitmapToBase64(imageAnalyzer.frame!!)
        val apiService = retrofit.create(ApiService::class.java)
        val call = apiService.verifyFace(Data(dni,base64Image))
        imageAnalyzer.isProcessing = false
        call.enqueue(object : Callback<MSG> {
            override fun onResponse(call: Call<MSG>, response: Response<MSG>) {
                val msg:MSG? = response.body()
                runOnUiThread(Runnable {

                    if(msg != null) {
                        progressDialog.dismiss()
                        Toast.makeText(baseContext, msg.result, Toast.LENGTH_LONG).show()
                        in_dni.text.clear()

                    }else{
                        runOnUiThread {
                            progressDialog.dismiss()
                            Toast.makeText(baseContext, "no se octuvo respuesta", Toast.LENGTH_LONG).show()
                        }
                    }
                    txt_dni.text=""
                    count = 0
                })
            }
            override fun onFailure(call: Call<MSG>, t: Throwable) {
                runOnUiThread(Runnable {
                    progressDialog.dismiss()
                    Toast.makeText(baseContext,"Algo fallo en el envio",Toast.LENGTH_LONG).show()
                })
                txt_dni.text=""
                count = 0
            }
        })
    }
}
data class Data(
    val dni:String="",
    val base64_image:String=""
)
data class MSG(
    @SerializedName("result")
    val result : String=""
    //@SerializedName("result")
    //val status:String = ""
)
interface ApiService {
    @POST("/api/v1.0/facialbiometric/validar/")
    fun verifyFace(@Body request: Data): Call<MSG>

    @POST("/api/v1.0/facialbiometric/obtener/")
    fun obtener(@Body request: Data):Call<MSG>
}

