package com.example.demo_facial

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class MyDialogFragment :DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val alertDialogBuilder = AlertDialog.Builder(requireActivity())
        alertDialogBuilder.setTitle("esperando respuesta")
        alertDialogBuilder.setMessage("Mensaje del cuadro de diálogo")
        alertDialogBuilder.setPositiveButton("Aceptar") { dialog, which ->

        }
        alertDialogBuilder.setNegativeButton("Cancelar") { dialog, which ->

        }
        return alertDialogBuilder.create()
    }
}